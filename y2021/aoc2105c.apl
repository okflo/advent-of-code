
)copy 5 FILE_IO

∇ init
  input←FIO∆read_lines 'aoc2105cc.txt'
  input← { (~⍵∊' ->')⊂⍵ } ¨ input
  input← { { {⍎⍵} ¨(~','=⍵)⊂⍵ } ¨ ⍵ } ¨ input
∇

∇ z←hor_vert input
  z←({ ((↑⍵[1])[1] = (↑⍵[2])[1])∨((↑⍵[1])[2] = (↑⍵[2])[2]) }¨input)/input
∇

∇ z← diag input
  z←({ =/|(↑⍵) - ↑¯1↑⍵ }¨input)/input
∇

⍝ ∇ z←draw input; ⎕IO
⍝   ⎕IO←0
⍝   z←output← 1000 1000 ⍴ 0
⍝   z←{ {((⌽⍵)⌷output)←1}¨⍵} ¨ input
⍝ ∇

⍝ ∇ z← draw_line in;p1;p2;x1;x2;y1;y2;dx;dy;⎕io
⍝   ⎕io←0
⍝   ⍝ https://en.wikipedia.org/wiki/Line_drawing_algorithm
⍝   in←in[⍋in]
⍝   (p1 p2)←in
⍝   (x1 y1)←p1
⍝   (x2 y2)←p2
⍝   dx←x2-x1
⍝   dy←y2-y1
⍝   z←{⍵ (y1+dy×(⍵-x1)÷dx) } ¨ x1+⍳1+dx
⍝ ∇

∇ z← draw_line in;p1;p2
  in←in[⍋in]
  (p1 p2)←in
  a←p2-p1
  a←a>0
  u←p1
  z←{⍵-a}¨({ u←u+a⊣⍵ } ¨ (⍳1++/p2-p1))
∇

∇ z←draw in; ⎕IO
  ⎕IO←0
  n←1
  z←output← 1000 1000 ⍴ 0
  z← { { ((⌽⍵)⌷output)←((⌽⍵)⌷output)+1 }¨(draw_line ⍵ ⊣ ⎕←n←n+1)} ¨ in
  ⍝z←+/+/output>1
∇

∇ z← draw_line_dia in; step; n; u
  in←in[⍋in]
  (p1 p2)←in
  n← ↑(|p1-p2)[1]
  step← ×p2-p1
  u ← p1
  z←(⊂p1),{ u ← u + step ⊣⍵}¨⍳n
∇

∇ z←draw_dia in; ⎕IO
  ⎕IO←0
  n←1
  z← { { ((⌽⍵)⌷output)←((⌽⍵)⌷output)+1 }¨(draw_line_dia ⍵ ⊣ ⎕←n←n+1)} ¨ in
  z←+/+/output>1
∇
