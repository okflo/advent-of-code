

)copy 5 FILE_IO



∇ init
  input ← FIO∆read_lines 'aoc2105.txt'
  draw_numbers ← (~','=↑input)⊂↑input ⍝ the drawn numbers
  draw_numbers ← {⍎⍵}¨draw_numbers
  input←{(~⍵=' ')⊂⍵}¨1↓input
  boards←{⊃⍵}¨({~⍵≡''}¨(1↓input))⊂1↓input
  boards←{{⍎⍵}¨⍵}¨boards
  boards←{(5 5 ⍴ 0),[0.5]⍵} ¨ boards ⍝ all boards setup with binary mask plane
∇

∇ z←boards mark_board number
  z←{ ⍵⊣⍵[1;;]← ⍵[1;;]∨⍵[2;;]=number}¨boards
∇

∇ z←check_board board
  z←+/5=(+⌿board[1;;]),+/board[1;;]
  ⍝z←+⌿board[1;;],5=+/board[1;;]
∇

∇ loop
  number←0
  final←0
start:
  ⎕←↑draw_numbers
  boards←boards mark_board ↑draw_numbers
  →((+/{check_board ⍵} ¨ boards)>number)/finish
  draw_numbers←1↓draw_numbers
  →start
finish:
  'ende:'
  ⎕←n←↑draw_numbers
  ⎕←number
  final←last
  last←({check_board ⍵} ¨ boards)/boards
  ⎕←boards∊last
  m←(({check_board ⍵} ¨ boards)>0)/({ +/+/(~⍵[1;;])×⍵[2;;] } ¨ boards)
  ⎕←n×m
  number←number+1
  →(number<⍴boards)/start
  →0
∇
  
∇ loop2
start:
  ⎕←↑draw_numbers
  boards←boards mark_board ↑draw_numbers
  ⎕←(+/(({ check_board ⍵ } ¨ boards)>0))
  ⎕←(({ check_board ⍵ } ¨ boards)>0)
  →((+/(({ check_board ⍵ } ¨ boards)>0))=100)/final
  draw_numbers←1↓draw_numbers
  →start
final:
  ⎕←↑draw_numbers
∇
  
