

(defparameter *codes* nil)

(defun read-data (file)
  (with-open-file (f file :direction :input )
    (loop for i in
                (loop for line = (read-line f nil nil)
                      while line
                      collect line)
          collect (loop for ii across i
                        collect
                        (if (equal ii #\1)
                            1
                            0)))))

