﻿ z←val_hgt val
 :If 'cm'≡¯2↑val
     :If (⍳0)≡('^[0-9]{3}$'⎕S 1)¯2↓val
         z←0
     :ElseIf 3=('^[0-9]{3}$'⎕S 1)¯2↓val
         z←(150≤(⍎¯2↓val))∧(⍎¯2↓val)≤193
     :Else
         z←0
     :EndIf
 :ElseIf 'in'≡¯2↑val
     :If (⍳0)≡('^[0-9]{2}$'⎕S 1)¯2↓val
         z←0
     :ElseIf 2=('^[0-9]{2}$'⎕S 1)¯2↓val
         z←(59≤(⍎¯2↓val))∧(⍎¯2↓val)≤76
     :Else
         z←0
     :EndIf
 :Else
     z←0
 :EndIf
